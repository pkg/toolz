Source: toolz
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Diane Trout <diane@ghic.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               furo,
               python3-docutils <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-all,
               python3-pytest <!nocheck>,
               python3-setuptools
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/toolz
Vcs-Git: https://salsa.debian.org/python-team/packages/toolz.git
Homepage: https://github.com/pytoolz/toolz/
Rules-Requires-Root: no

Package: python3-toolz
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-toolz-doc
Description: List processing tools and functional utilities
 A set of utility functions for iterators, functions, and
 dictionaries.  These functions interoperate well and form the
 building blocks of common data analytic operations. They extend the
 standard libraries itertools and functools and borrow heavily from
 the standard libraries of contemporary functional languages.

Package: python-toolz-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: List processing tools and functional utilities documentation
 A set of utility functions for iterators, functions, and
 dictionaries.  These functions interoperate well and form the
 building blocks of common data analytic operations. They extend the
 standard libraries itertools and functools and borrow heavily from
 the standard libraries of contemporary functional languages.
 .
 This contains the documentation
